/*
Author: de Freitas, P.C.P
References
[1]Covariance, wikipedia, https://pt.wikipedia.org/wiki/Covari%C3%A2ncia
*/
#include<iostream>
using namespace std;
double mean(double data[], int length)
{
	double sum = 0.0;
	for (size_t i = 0; i < length; i++)
	{
		sum += data[i];
	}
	return sum / length;
}
double populationCovariance(double a[], double b[], int length)
{
	double sum = 0.0;
	for (size_t i = 0; i < length; i++)
	{
		sum += (a[i] - mean(a, length))*(b[i] - mean(b, length));
	}
	return sum / length;
}
double sampleCovariance(double a[], double b[], int length)
{
	double sum = 0.0;
	for (size_t i = 0; i < length; i++)
	{
		sum += (a[i] - mean(a, length))*(b[i] - mean(b, length));
	}
	return sum / length - 1;
}
int main()
{
	double CDAM20181[24] = { 1.49,1.09,0.96,1.22,1.24,1.32,1.21,0.99,0.96,1.06,
		1.02,1.05,1.02,0.96,1.18,0.99,0.98,1.86,0.99,1.15,1.21,1.08,1.06,0.96 };
	double NEAM20181[24] = { 10,7.5,7.5,0,8,0,10,6,8.5,0,8.25,6.5,5,8.5,0,5.5,0,9,8.5,9,0,0,5.5,0 };
	cout << "Mean CDAM20181 : " << mean(CDAM20181, 24) << endl;
	cout << "Mean NEAM20181 : " << mean(NEAM20181, 24) << endl;
	cout << "Sample Covariance : " << sampleCovariance(CDAM20181, NEAM20181, 24) << endl;
	cout << "Population Covariance : " << populationCovariance(CDAM20181, NEAM20181, 24) << endl;
	cout << endl;
	double CDAM20182[8] = { 1.02,0.99,0.84,1.06,1.02,1.02,0.99,0.99 };
	double NEAM20182[8] = { 7,8.25,8,8.75,6.5,2.25,7,4.5 };
	cout << "Mean CDAM20182 : " << mean(CDAM20182, 8) << endl;
	cout << "Mean NEAM20182 : " << mean(NEAM20182, 8) << endl;
	cout << "Sample Covariance : " << sampleCovariance(CDAM20182, NEAM20182, 8) << endl;
	cout << "Population Covariance : " << populationCovariance(CDAM20182, NEAM20182, 8) << endl;
	cout << endl;
	double CDES20181[11] = { 1.18,1.15,1.09,1.09,1.09,1.08,1.05,1.02,1.02,0.99,0.99 };
	double NEES20181[11] = { 0,0,7.5,0,8,0,4.5,7.5,3.5,6.5,7 };
	cout << "Mean CDES20181 : " << mean(CDES20181, 11) << endl;
	cout << "Mean NEES20181 : " << mean(NEES20181, 11) << endl;
	cout << "Sample Covariance : " << sampleCovariance(CDES20181, NEES20181, 11) << endl;
	cout << "Population Covariance : " << populationCovariance(CDES20181, NEES20181, 11) << endl;

	return 0;
}