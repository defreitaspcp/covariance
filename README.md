##Covariância
Em teoria da probabilidade e na estatística, a covariância, ou variância conjunta, é uma medida do grau de interdependência (ou inter-relação) numérica entre duas variáveis aleatórias. Assim, variáveis independentes têm covariância zero.
A covariância é por vezes chamada de medida de dependência linear entre as duas variáveis aleatórias.

##Referência
[1]Covariância, wikipedia ,https://pt.wikipedia.org/wiki/Covari%C3%A2ncia